package core;

import java.io.IOException;

public class GitCommitObject extends GitBlobObject{
	public GitCommitObject(String repoPath, String masterCommitHash) {
		super(repoPath, masterCommitHash);
	}
	
	public String getHash() {
		return super.getHash();
	}
	public String getTreeHash() throws IOException {
		String treeline = getContentLine(0);
		return treeline.split(" ")[1];
	}
	public String getParentHash() throws IOException {
		String parentLine = getContentLine(1);
		return parentLine.split(" ")[1];
	}

	public String getAuthor() throws IOException {
		String authorLine = getContentLine(2);
		return authorLine.substring(authorLine.indexOf(" ")+1,authorLine.indexOf(">")+1);
	}
	
	private String getContentLine(int parentIndexLine) throws IOException {
		String content = getContent();
		String parentLine = content.split("\n")[parentIndexLine];
		return parentLine;
	}
}
