package core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.InflaterInputStream;;
public class GitBlobObject {
	private String repoPath, objectName;
	public GitBlobObject(String repoPath, String objectName) {
		this.repoPath=repoPath;
		this.objectName=objectName;
	}
	public String getType() throws IOException, DataFormatException {
		String decompressed = inflateAndReadFromObject();
		return decompressed.split(" ")[0];
	}
	public String getContent() throws IOException {
		String decompressed = inflateAndReadFromObject();
		return decompressed.substring(decompressed.indexOf("\0")+1);
	}
	private String inflateAndReadFromObject() throws FileNotFoundException, IOException {
		FileInputStream in = new FileInputStream(repoPath+"/objects/"+objectName.substring(0, 2)+"/"+objectName.substring(2));
		InflaterInputStream inputStream = new InflaterInputStream(in);
		byte[] buf = new byte[in.available()];
		String decompressed=new String();
		int read=0;
		while((read=inputStream.read(buf))!=-1) {
			decompressed+=new String(buf,0,read);
		};
		inputStream.close();
		return decompressed;
	}
	public String getHash() {
		return this.objectName;
	}
}
