package core;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.DataFormatException;
import java.util.zip.InflaterInputStream;


public class GitTreeObject extends GitBlobObject{
	private Map<String,GitBlobObject> objects;
	public GitTreeObject(String repoPath, String objectHash) throws IOException, DataFormatException {
		super(repoPath, objectHash);
		this.objects = inflateAndReadFromObject(repoPath, objectHash);
	}
	private Map<String,GitBlobObject> inflateAndReadFromObject(String repoPath, String objectfile) throws IOException, DataFormatException { 
		Map<String,GitBlobObject> treeEntries = new HashMap<>();
		FileInputStream fis = new FileInputStream(repoPath+"/objects/"+objectfile.substring(0, 2)+"/"+objectfile.substring(2));
		  InflaterInputStream inStream = new InflaterInputStream(fis);
		  int i = -1;
		  while((i = inStream.read()) != 0){
		      //First line
		  }

		  //Content data
		  while((i = inStream.read()) != -1){
		    readPermissions(inStream);
		    
		    //Filename: 0-terminated
		    String filename = readFileName(inStream);
		    
		    //Hash: 20 byte long, can contain any value, the only way
		    String hash = readSHAcompressed(inStream);
		    addANewEntry(repoPath, treeEntries, filename, hash);
		    
		  }
		  inStream.close();
		  return treeEntries;
	}
	private void addANewEntry(String repoPath, Map<String, GitBlobObject> treeEntries, String filename, String hash)
			throws IOException, DataFormatException {
		GitBlobObject object = new GitBlobObject(repoPath,hash);
		if(object.getType().equalsIgnoreCase("tree")) {
				treeEntries.put(filename, new GitTreeObject(repoPath, hash));
		} else {
				treeEntries.put(filename, object);
		}
	}
	private void readPermissions(InflaterInputStream inStream) throws IOException {
		int i;
		while((i = inStream.read()) != 0x20){ //0x20 is the space char
		  //Permission bytes
		}
	}
	private String readFileName(InflaterInputStream inStream) throws IOException {
		int i;
		String filename = "";
		while((i = inStream.read()) != 0){
		  filename += (char) i;
		}
		return filename;
	}
	private String readSHAcompressed(InflaterInputStream inStream) throws IOException {
		int i;
		String hash = "";
		for(int count = 0; count < 20 ; count++){
		  i = inStream.read();
		  String singleHash = Integer.toHexString(i);
		  singleHash=String.format("%2s",singleHash);
		  hash += singleHash.replace(' ', '0');
		}
		return hash;
	}
	public List<String> getEntryPaths() throws IOException{
		return new ArrayList<>(this.objects.keySet());
	}
	
	public GitBlobObject getEntry(String entryPath) {
		return this.objects.get(entryPath);
	}
}
