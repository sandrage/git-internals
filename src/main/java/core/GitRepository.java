package core;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class GitRepository {
	private String repoPath;
	public GitRepository(String repoPath) {
		this.repoPath=repoPath;
	}
	
	public String getHeadRef() throws IOException {
		String fileName = "HEAD";
		String line = readFromFile(fileName);
		String[] splitted=line.split(" ");
		return splitted[1];
	}
	
	public String getRefHash(String path) throws IOException {
		String hash = this.readFromFile(path);
		return hash;
	}
	

	private String readFromFile(String fileName) throws FileNotFoundException, IOException {
		FileReader fileReader = new FileReader(repoPath+"/"+fileName);
		BufferedReader reader = new BufferedReader(fileReader);
		String refLine = reader.readLine();
		reader.close();
		return refLine;
	}
}
